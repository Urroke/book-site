import React, { Component } from 'react';
import './App.css';
import Head from './lib/Head';

class App extends Component {
  render() {
    return (
        <div>
          <Head />
        </div>
    );
  }
}

export default App;
