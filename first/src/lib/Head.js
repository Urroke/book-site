import React, {Component} from 'react';
import HeadBlock from './HeadBlock';
import './style.css';

class Head extends Component
{
    render(){
        return(
            <div>
                <div className = "head">
                    <HeadBlock value = "Main"/>
                    <HeadBlock value = "About"/>
                    <HeadBlock value = "Tie"/>
                </div>
            </div>
        );
    }
}

export default Head;