import React, { Component } from 'react';
import './style.css';


class HeadBlock extends Component
{
    render(){
        const { value } = this.props;
        return(
            <div className="headBlock">
                {value}
            </div>
        );
    }
}

export default HeadBlock;